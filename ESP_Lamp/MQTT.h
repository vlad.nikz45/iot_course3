#include <PubSubClient.h>

extern WiFiClient wifi_client;

PubSubClient mqtt_client(wifi_client);

String mqttMessage;


String id() {
  uint8_t mac[WL_MAC_ADDR_LENGTH];
  int last = WL_MAC_ADDR_LENGTH - 1;
  int prelast = WL_MAC_ADDR_LENGTH - 2;
  WiFi.softAPmacAddress(mac);
  String id = String(mac[prelast], HEX) + String(mac[last, HEX]);
  return id;
}


void callback(char* topic, byte* payload, unsigned int length) {
  mqttMessage = "";
  Serial.println("Message received on topic: ");
  Serial.println(topic);
  Serial.println("Message is: ");
  for(int i = 0; i < length; i++) {
    mqttMessage += (char)payload[i];
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if (mqttMessage == "HIGH") {

      bool current = digitalRead(led_pin);
      digitalWrite(led_pin, LOW);
      Serial.println("led on");
    } 
    else if (mqttMessage == "LOW") { 
      digitalWrite(led_pin, HIGH);
      Serial.println("led off");
    }
}


void init_MQTT() {
    mqtt_client.setServer(mqtt_broker, mqtt_port);
    mqtt_client.setCallback(callback);
    while(!mqtt_client.connected()) {
      Serial.println("Trying to connect ");
      String client_id = "esp8266-" + id();
      bool isSuccess = mqtt_client.connect(client_id.c_str());
      if (isSuccess) {
        Serial.println("Successfully connected to " + client_id);
      } else {
        Serial.println("Failed to connect with " + client_id);
        Serial.println(mqtt_client.state());
        delay(2000);
      }
    }
}