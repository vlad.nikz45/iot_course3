#include <ESP8266WiFi.h>
#include "Config.h"
#include "WIFI.h"
#include "server.h"



void setup() {
  Serial.begin(9600);
  pinMode(led_pin, OUTPUT);
  // start_AP_mode();
  // init_WiFi(false);
  server_init();
  // init_MQTT();
  // String state_topic = "esp8266-" + id() + "/state";
  // String command_topic = "esp8266-" + id() + "/command";
  // String state_topic = "vlados/state";
  // String command_topic = "vlados/command";
  // mqtt_client.publish(state_topic.c_str(), "Hello");
  // mqtt_client.subscribe(command_topic.c_str());
}




bool isInternetAvailable() {
  WiFiClient client;
  if (client.connect("www.google.com", 80)) {
    
    client.stop();
    return true;
  } else {
    
    client.stop();
    return false;
  }
}

bool internetConnected = false; 

void loop() {
  server.handleClient();
  if (!internetConnected) { 
    if (WiFi.status() != WL_CONNECTED) { 
      start_AP_mode(); 
      internetConnected = false; 
    } else { 
      if (isInternetAvailable()) { 
        internetConnected = true; 
      } else {
        start_AP_mode(); 
        internetConnected = false; 
      }
    }
  } else { 
    if (WiFi.status() != WL_CONNECTED) { 
      start_AP_mode(); 
      internetConnected = false; 
    } else {
      if (!isInternetAvailable()) { 
        start_AP_mode(); 
        internetConnected = false; 
      }
    }
  }

  mqtt_client.loop(); 
  delay(10);
}
