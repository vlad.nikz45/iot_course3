import random
import time
from paho.mqtt import client as mqtt_client
import argparse

broker = 'broker.hivemq.com'
username = 'emqx'
password = 'public'

def connect_mqtt(client_id, topic):
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print(f"Failed to connect, return code {rc}")

    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker)
    return client

def publish(client, on_time,topic):
    current_minute = int(time.strftime("%M"))

    while True:
        current_second = int(time.strftime("%S"))
        print(current_second, '---', on_time)
        if (20 <= current_second <= on_time):
            msg = "HIGH"
        else:
            msg = "LOW"

        result = client.publish(topic, msg)
        status = result.rc
        if status == mqtt_client.MQTT_ERR_SUCCESS:
            print(f"Send `{msg}` to topic `{topic}`")
        else:
            print(f"Failed to send message to topic {topic}")

        time.sleep(1)

        if int(time.strftime("%M")) != current_minute:
            current_minute = int(time.strftime("%M"))
            on_time -= 1

            if on_time < 30:
                on_time = 40

def run():
    parser = argparse.ArgumentParser(description='MQTT Lamp Control Client')
    parser.add_argument('topic', help='Topic (ID) for the lamp')
    args = parser.parse_args()

    client_id = f'python-mqtt-{random.randint(0, 1000)}'
    topic = args.topic

    client = connect_mqtt(client_id, topic)
    client.loop_start()
    publish(client, 40,topic)

if __name__ == '__main__':
    run()