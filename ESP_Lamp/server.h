#include <ESP8266WebServer.h>


ESP8266WebServer server(web_port);


// void handle_root() {
//   String page_code = "<form action=\"/LED\" method=\"POST\">";
//   page_code += "<input type=\"submit\" value=\"Switch LED\">";
//   page_code += "</form>";
//   page_code += "<div>" + String(!digitalRead(led_pin)) + "</div>";
//   page_code += "<div>" + String(analogRead(sensor_pin)) + "</div>";

//   server.send(200, "text/html", page_code);
// }

void handle_root() {
  String page_code = "<form action=\"/configure\" method=\"POST\">";
  page_code += "SSID: <input type=\"text\" name=\"ssid\"><br>";
  page_code += "Password: <input type=\"password\" name=\"password\"><br>";
  page_code += "<input type=\"submit\" value=\"Submit\">";
  page_code += "</form>";

  server.send(200, "text/html", page_code);
}


void handle_led() {
  Serial.print("-----");
  bool current = digitalRead(led_pin);
  // digitalWrite(led_pin, !current);
  server.sendHeader("/LED", "/");

  String page_code = "<div>" + String(analogRead(sensor_pin)) + "</div>";

  server.send(303, "text/html", page_code);
}


void handle_configure() {
  String message = "Number of args received:";
  message += server.args();           
  message += "\n";  
  bool isValideLogin = false;
  bool isValidePass = false;
  bool mode_AP = true;                         

  for (int i = 0; i < server.args(); i++) {

    // message += "Arg nº" + (String)i + " –> ";  
    message += server.argName(i) + ": ";     
    message += server.arg(i) + "\n";             
    if (i == 0) {
      if (server.arg(i) == ssid_client) {
          isValideLogin = true;
      }
    } else if (i == 1) {
      if (server.arg(i) == pass_client) {
          isValidePass = true;
      }
    }
    

  } 
  if (isValideLogin && isValidePass) {
      Serial.println("Success!");
      mode_AP = false;
    } else {
      Serial.println("Failed!");
    }
  server.send(200, "text/plain", message);      
  init_WiFi(mode_AP);
  Serial.println(message);
}


void server_init() {
  server.on("/", HTTP_GET, handle_root);
  server.on("/configure", HTTP_POST, handle_configure);
  // server.on("/LED", HTTP_POST, handle_led);

  server.begin();
  Serial.println("HTTP Server is on, on port" + String(web_port));
}
